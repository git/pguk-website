# -*- coding: utf-8 -*-
import os
from django.conf import settings

from postgresqleu.util.misc.baseinvoice import BaseInvoice, BaseRefund


class SELBase(object):
    logo = os.path.join(settings.PROJECT_ROOT, '../media/img/PostgreSQL_logo.1color_blue.300x300.png')
    headertext = """Slonik Enterprises Ltd.
128 City Road
London
EC1V 2NX

Company number: 14737028
"""
    sendertext = """Your contact:
    
Slonik Enterprises Treasurer
treasurer@slonik.enterprises"""

    bankinfotext = """Name: Slonik Enterprises Ltd
IBAN: GB49 TRWI 2314 7077 8042 80
Account: 77804280
Sort Code: 23-14-70
Bank: Wise Payments Ltd
"""

    paymentterms = """Slonik Enterprises Ltd is not VAT registered and does not charge VAT.
Discount for prepayment: None.
"""


class SELInvoice(SELBase, BaseInvoice):
    pass


class SELRefund(SELBase, BaseRefund):
    pass
