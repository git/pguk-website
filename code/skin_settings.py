import os

SKIN_APPS = [

]

SITEBASE = "https://pgday.uk"
ORG_NAME = "PostgreSQL Europe"
ORG_SHORTNAME = "PGUK"

# Modules
ENABLE_PG_COMMUNITY_AUTH = True
ENABLE_MEMBERSHIP = False
ENABLE_ELECTIONS = False

ENABLE_AUTO_ACCOUNTING = True
ACCOUNTING_MANUAL_INCOME_ACCOUNT = 1900
ACCOUNTING_CONFREG_ACCOUNT = 3003
ACCOUNTING_CONFSPONSOR_ACCOUNT = 3004

# Emails
DEFAULT_EMAIL = "webmaster@pgday.uk"
SERVER_EMAIL = "webmaster@pgday.uk"
TREASURER_EMAIL = "treasurer@slonik.enterprises"
INVOICE_SENDER_EMAIL = "treasurer@slonik.enterprises"
INVOICE_NOTIFICATION_RECEIVER = "treasurer@slonik.enterprises"
SCHEDULED_JOBS_EMAIL = "webmaster@pgday.uk"

# Ugh
EU_VAT = False
EU_VAT_HOME_COUNTRY = "GB"
EU_VAT_VALIDATE = False

# Invoice
INVOICE_PDF_BUILDER = 'selinvoices.SELInvoice'
REFUND_PDF_BUILDER = 'selinvoices.SELRefund'
